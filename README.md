# codility-PermMissingElem

This is a lesson in codility for the Time Complexity algorithm. Given an array of integer, you need to find the lowest missing integer.